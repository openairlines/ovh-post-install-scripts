# OVH post-install scripts

## Description

This _public_ repository keeps track of our OVH post-installation script.
As we also use Ansible, this script is kept to the strict minimum: operations
that might be done _ONLY ONCE_ and that are potentially dangerous for production
data.

## Tagging

Tagging is done manually using `YEAR.MONTH.X` with X starting to _01_ and
incrementing each time we want to use a newer version.

When we want to use a new tag, we _MUST_ update the OVH template while
installing a new server and _manually update_ the URL of the post-installation
script.
