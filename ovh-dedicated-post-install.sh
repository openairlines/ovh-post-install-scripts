#!/bin/bash

nb_disks=$(lsblk -d -n | grep disk | wc -l)
if [ $nb_disks -eq 4 ]
then

    sata_disks=$(lsblk -p -o NAME,TRAN /dev/sd*  | grep " sata$" | awk '{print $1}')
    for d in sata_disks
    do
        parted --script -- $d mkpart primary 1024MB -1
    done

    yes | mdadm --create /dev/md10 --level=1 --raid-devices=2 $sata_disks
    /usr/share/mdadm/mkconf > /etc/mdadm/mdadm.conf
    vgcreate vgdata /dev/md10
fi
fstrimPid=$(lsof | grep /dummy | awk '/fstrim/ {print $2}')
if [ -n "$fstrimPid" ]
then
   tail --pid="$fstrimPid" -f /dev/null
fi
umount /dev/vg/dummy
sed -i -e "/\/dev\/vg\/dummy/d" /etc/fstab
lvremove -f vg/dummy
